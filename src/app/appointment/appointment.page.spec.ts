import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AppointmentPage } from './appointment.page';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

describe('AppointmentPage', () => {
  let component: AppointmentPage;
  let fixture: ComponentFixture<AppointmentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppointmentPage],
      imports: [ReactiveFormsModule, FormsModule, IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AppointmentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.ngOnInit();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.appointmentForm.valid).toBeFalsy();
  })

});
