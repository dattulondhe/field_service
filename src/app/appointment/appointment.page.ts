import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { DatabaseService } from '../services/database.service';
import { CommonService } from '../services/common.service';
@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.page.html',
  styleUrls: ['./appointment.page.scss'],
})
export class AppointmentPage implements OnInit {
  public appointmentForm: FormGroup; //declared the formname of type FornGroup
  public submited: boolean = false;
  // public minDate =moment().format('YYYY-MM-DD') // if required start date validation from today
  public maxDate: any; // for validation of end date
  constructor(private fb: FormBuilder, private database: DatabaseService, private common: CommonService) { }

  ngOnInit() {
    //defining the form controls and validations
    this.appointmentForm = this.fb.group({
      summary: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(255)])),
      location: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(255)])),
      startDate: new FormControl('', Validators.compose([Validators.required])),
      endDate: new FormControl('', Validators.compose([Validators.required]))
    });
  }

  startDateChanged(event) {
    /** forcing next date to end date control 
     custom js functions
    */

    // this.maxDate = new Date(this.appointmentForm.value.startDate);
    // this.maxDate.setDate(this.maxDate.getDate() + 1) // append a day to max date
    // let dd = this.maxDate.getDate();
    // let mm = this.maxDate.getMonth() + 1; // +1 hence its array of month
    // let y = this.maxDate.getFullYear();
    // this.maxDate = y + '-' + mm + '-' + dd; // converting to date string

    // by using moment lib adding next date 
    // this.maxDate = moment(this.appointmentForm.value.startDate).add(1, 'days').format('YYYY-MM-DD'); // if next day date required uncomment this line

    //returning the same date
    this.maxDate = moment(this.appointmentForm.value.startDate).format('YYYY-MM-DD');
  }

  onClickSubmit() {
    this.submited = true;
    if (this.appointmentForm.invalid) {
      return; // of form not valid 
    }
    this.database.setData(this.appointmentForm.value).then(async (res) => {
      if (res) {
        this.common.presetToast('Appointment created!'); // shown toast from common service
        this.resetFrom(); // reset the form if appo created.
      }
    })
  }

  resetFrom() {
    // resetting the form and validation
    this.appointmentForm.reset();
    this.submited = false;
  }

}
