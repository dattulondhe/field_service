import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { DatabaseService } from '../services/database.service';
import { CommonService } from '../services/common.service';
import * as moment from 'moment';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.page.html',
  styleUrls: ['./edit-modal.page.scss'],
})
export class EditModalPage implements OnInit {
  public appointmentEditForm: FormGroup; //declared the formname of type FornGroup
  public submited: boolean = false;
  public maxDate: any
  constructor(private navParams: NavParams, private modalController: ModalController, private fb: FormBuilder, private database: DatabaseService, private common: CommonService) {
    this.appointmentEditForm = this.fb.group({
      summary: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(255)])),
      location: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(255)])),
      startDate: new FormControl('', Validators.compose([Validators.required])),
      endDate: new FormControl('', Validators.compose([Validators.required]))
    });
  }

  startDateChanged(event) {
    this.maxDate = moment(this.appointmentEditForm.value.startDate).format('YYYY-MM-DD');
  }

  ngOnInit() {
    this.appointmentEditForm.controls.summary.setValue(this.navParams.data.desc);
    this.appointmentEditForm.controls.location.setValue(this.navParams.data.title);
    this.appointmentEditForm.controls.startDate.setValue(moment(this.navParams.data.startTime).format('YYYY-MM-DD HH:MM:ss'));
    this.appointmentEditForm.controls.endDate.setValue(moment(this.navParams.data.endTime).format('YYYY-MM-DD HH:MM:ss'));
  }

  async closeModal(param) {
    this.submited = false;
    await this.modalController.dismiss(param);
  }

  onClickUpdate() {
    this.submited = true;
    if (this.appointmentEditForm.invalid) {
      return;
    }
    this.database.updateData(this.navParams.data.appointmentId, this.appointmentEditForm.value).then((res) => {
      if (res) {
        this.common.presetToast("Appointment updated!");
        this.closeModal(true);
      }
    })
  }

}
