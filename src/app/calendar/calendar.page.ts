import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../services/database.service';
import * as moment from 'moment';
import { ApplicationRef } from '@angular/core'
import { CommonService } from '../services/common.service';
import { ModalController } from '@ionic/angular';
import { EditModalPage } from '../edit-modal/edit-modal.page';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.page.html',
  styleUrls: ['./calendar.page.scss'],
})
export class CalendarPage implements OnInit {
  public eventSource = [];
  public monthTitle: string; // current month titlte 

  public calendar = {
    mode: 'month',
    currentDate: new Date(),
  }; // default setting of calendar

  constructor(private database: DatabaseService, private applicationRef: ApplicationRef, private common: CommonService, public modalController: ModalController) { }

  public loadEvents() {
    this.eventSource = [];
    let self = this, events = [];
    this.database.getData().then((records: any) => {
      if (records.length) {
        records.forEach(record => {
          events.push({
            title: record.location,
            desc: record.summary,
            endTime: moment(record.startDate).toDate(),
            startTime: moment(record.endDate).toDate(),
            allDay: true,
            appointmentId: record.appointmentId
          });
        });
      };
    })
    setTimeout(() => {
      self.eventSource = events;
      self.applicationRef.tick();
      console.log('events', self.eventSource);
    }, 100);
  }

  ngOnInit() {
    this.loadEvents();
  }

  public next() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slideNext();
  }

  public back() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slidePrev();
  }

  // Focus today
  public today() {
    this.calendar.currentDate = new Date();
  }

  // Selected date reange and hence title changed
  public onViewTitleChanged(title) {
    this.monthTitle = title;
  }

  public onEventSelected(event) {
    console.log(event);
    this.common.showAlert(event.title, event.desc)
  }

  public async editEvent(event) {
    console.log(event);
    const modal = await this.modalController.create({
      component: EditModalPage,
      componentProps: event
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data) {
        this.loadEvents();
      }
    });

    return await modal.present();
  }

  public deleteEvent(event) {
    this.common.confirm("Are you sure, You want to delete this appointment?").then(res => {
      if (res) {
        this.database.deleteData(event.appointmentId).then(res => {
          if (res) {
            this.common.presetToast('Appointment deleted!');
            this.loadEvents();
          }
        });
      }
    })


  }


}
