import { Injectable } from '@angular/core';
import { ToastController, AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private toast: ToastController, public alertController: AlertController) { }

  async presetToast(msg) {
    const toast = await this.toast.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  async showAlert(title, content) {
    const alert = await this.alertController.create({
      header: title,
      message: content,
      buttons: ['OK']
    });
    await alert.present();
  }

  confirm(msg) {
    return new Promise(async (resolve) => {
      const alert = await this.alertController.create({
        header: 'Confirm!',
        message: msg,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              resolve(false);
            }
          }, {
            text: 'Okay',
            handler: () => {
              resolve(true);
            }
          }
        ]
      });
      (await alert).present();
    })
  }

}
