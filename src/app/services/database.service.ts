import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  /*
  in the real app we can use sqlite for larger Database if required.
  here i create on object to store the records for example purpose.
  */

  private dataObj: any[] = []; // init blank  array object;

  constructor() { }

  setData(record) {
    record.appointmentId = this.dataObj.length; // added appointmentId for further use
    return new Promise((resolve, reject) => {
      this.dataObj.push(record);
      resolve(true);
    })
  }

  getData() {
    return new Promise((resolve, reject) => {
      resolve(this.dataObj);
    })
  }

  updateData(appointmentId, record) {
    record.appointmentId = appointmentId;
    return new Promise((resolve, reject) => {
      this.dataObj[appointmentId] = record;
      resolve(true);
    })
  }

  deleteData(index) {
    return new Promise((resolve, reject) => {
      this.dataObj.splice(index, 1);
      resolve(true);
    })
  }


}
